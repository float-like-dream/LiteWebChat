package com.socket.secure.config;

import com.socket.secure.constant.SecureProperties;
import com.socket.secure.filter.validator.RepeatValidator;
import com.socket.secure.filter.validator.impl.MappedRepeatValidator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@Configuration
public class RepeatValidatorConfig {
    private final SecureProperties properties;

    public RepeatValidatorConfig(SecureProperties properties) {
        this.properties = properties;
    }

//    @Bean
//    @ConditionalOnMissingBean
//    @ConditionalOnClass(RedisTemplate.class)
//    public RepeatValidator redisRepeatValidator(StringRedisTemplate template) {
//        return new RedisRepeatValidator(template, properties);
//    }

    @Bean
    @ConditionalOnMissingBean
    public RepeatValidator repeatValidator() {
        File cache = new File(System.getProperty("java.io.tmpdir"), "MapRepeatValidatorData");
        return new MappedRepeatValidator(cache, properties);
    }
}
