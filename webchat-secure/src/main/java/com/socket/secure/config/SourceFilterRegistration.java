package com.socket.secure.config;

import com.socket.secure.filter.EnableFilter;
import com.socket.secure.filter.SecureRequsetFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.Collections;

/**
 * Security request filter configuration
 */
@Configuration
public class SourceFilterRegistration {

    @Bean
    @Conditional(EnableFilter.class)
    public FilterRegistrationBean<Filter> MyRegistrationBean(SecureRequsetFilter filter) {
        FilterRegistrationBean<Filter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(filter::filter);
        registrationBean.setUrlPatterns(Collections.singleton("/*"));
        registrationBean.setOrder(1);
        return registrationBean;
    }
}