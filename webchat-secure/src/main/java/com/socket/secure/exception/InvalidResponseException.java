package com.socket.secure.exception;

import com.socket.secure.constant.RequsetTemplate;

/**
 * This exception is thrown when the client returns an encrypted response without exchanging keys <br>
 *
 * @date 2023/4/2
 */
public class InvalidResponseException extends RuntimeException {
    public InvalidResponseException() {
    }

    public InvalidResponseException(String message) {
        super(message);
    }

    public InvalidResponseException(RequsetTemplate template, Object... objs) {
        super(template.format(objs));
    }
}
