package com.socket.secure.exception;

import com.socket.secure.constant.RequsetTemplate;

/**
 * This exception is thrown when parsing unsupported request type
 *
 * @date 2023/4/4
 */
public class RequestNotSupportedException extends InvalidRequestException {
    public RequestNotSupportedException(RequsetTemplate template, Object... objs) {
        super(template, objs);
    }
}
