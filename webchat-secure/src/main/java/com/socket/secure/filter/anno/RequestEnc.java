package com.socket.secure.filter.anno;

import java.lang.annotation.*;

/**
 * Encrypt the request content of this interface <br>
 * Mark this annotation to require the client to present
 * the correct signature to access the controller,
 * you can modify the signature key by {@link #sign()}
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestEnc {
    /**
     * Client request signature ID
     */
    String sign() default "sign";
}
