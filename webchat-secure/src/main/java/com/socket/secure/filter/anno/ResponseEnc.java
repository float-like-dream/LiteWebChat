package com.socket.secure.filter.anno;

import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.annotation.*;

/**
 * Encrypt the response content of this interface <br>
 * By default, clients that do not exchange keys will return an exception,
 * You can turn off the {@link #throwIfNotKey()} configuration,
 * and the client will return clear text results if it does not exchange keys<br>
 * Note: Do not mark this annotation on methods that return content inclusion bytes,
 * as they do not support cryptographic operations
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@ResponseBody
public @interface ResponseEnc {
    /**
     * if the client did not exchange keys,
     * turning off this configuration returns a clear text response
     */
    boolean throwIfNotKey() default true;
}
