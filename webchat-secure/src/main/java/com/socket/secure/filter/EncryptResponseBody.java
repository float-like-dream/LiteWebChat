package com.socket.secure.filter;

import com.socket.secure.exception.InvalidResponseException;
import com.socket.secure.filter.anno.ResponseEnc;
import com.socket.secure.util.AESUtil;
import com.socket.secure.util.Assert;
import com.socket.secure.util.MappingUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import javax.servlet.http.HttpSession;

/**
 * {@link javax.servlet.http.HttpServletResponse} cryptographic processing
 */
@ControllerAdvice
public class EncryptResponseBody implements ResponseBodyAdvice<Object> {
    private HttpSession session;

    @Override
    public boolean supports(MethodParameter parameter, Class<? extends HttpMessageConverter<?>> convert) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter parameter, MediaType mediaType,
                                  Class<? extends HttpMessageConverter<?>> convert,
                                  ServerHttpRequest request, ServerHttpResponse response) {
        ResponseEnc anno = parameter.getDeclaringClass().getAnnotation(ResponseEnc.class);
        if (anno == null) {
            anno = parameter.getMethodAnnotation(ResponseEnc.class);
        }
        if (anno == null) {
            return body;
        }
        String aesKey = AESUtil.getAesKey(session);
        Assert.isFalse(anno.throwIfNotKey() && aesKey == null, InvalidResponseException::new);
        return AESUtil.encrypt(MappingUtil.toJSONString(body), aesKey);
    }

    @Autowired
    public void setSession(HttpSession session) {
        this.session = session;
    }
}