package com.socket.client.feign.param;

import lombok.Data;

@Data
public class UserParam {
    /**
     * 用户id
     */
    private String guid;
    /**
     * 是否隐藏所属省份
     */
    private boolean hideProvince;
}
