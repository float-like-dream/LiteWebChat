package com.socket.client.feign;

import com.socket.client.feign.param.UserParam;
import com.socket.client.model.chat.SysUser;
import com.socket.client.model.vo.FeignVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "webchat-server", path = "/user")
public interface SysUserApi {
    /**
     * 获取用户详情
     */
    @PostMapping("/detail")
    FeignVo<SysUser> detail(UserParam param);
}
