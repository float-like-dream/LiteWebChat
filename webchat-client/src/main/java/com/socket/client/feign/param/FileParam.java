package com.socket.client.feign.param;

import lombok.Data;

@Data
public class FileParam {
    /**
     * 文件
     */
    private String base64;
    /**
     * 绑定的消息ID
     */
    private String msgId;
}
