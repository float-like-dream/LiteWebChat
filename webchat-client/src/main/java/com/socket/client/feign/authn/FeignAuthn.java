package com.socket.client.feign.authn;

import com.socket.core.constant.ChatConstants;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

/**
 * Feign插入请求认证头标识
 */
@Configuration
@RequiredArgsConstructor
public class FeignAuthn implements RequestInterceptor {
    private final ChatConstants constants;

    @Override
    public void apply(RequestTemplate template) {
        template.header(constants.getOauthToken(), constants.getOauthTokenValue());
    }
}
