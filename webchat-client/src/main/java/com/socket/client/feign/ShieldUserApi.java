package com.socket.client.feign;

import com.socket.client.feign.param.ShieldParam;
import com.socket.client.model.vo.FeignVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@FeignClient(name = "webchat-server", path = "/shield")
public interface ShieldUserApi {
    /**
     * 获取屏蔽的用户列表
     */
    @PostMapping("/list")
    FeignVo<List<String>> getShieldList(ShieldParam param);
}
