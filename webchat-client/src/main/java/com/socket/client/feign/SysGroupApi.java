package com.socket.client.feign;

import com.socket.client.feign.param.GroupParam;
import com.socket.client.model.chat.SysGroup;
import com.socket.client.model.vo.FeignVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "webchat-server", path = "/group")
public interface SysGroupApi {
    /**
     * 获取群组详情
     */
    @PostMapping("/detail")
    FeignVo<SysGroup> detail(GroupParam param);
}
