package com.socket.client.feign.param;

import lombok.Data;

@Data
public class GroupParam {
    /**
     * 群组id
     */
    private String gid;
}
