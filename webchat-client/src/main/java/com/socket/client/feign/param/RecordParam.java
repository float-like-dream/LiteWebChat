package com.socket.client.feign.param;

import lombok.Data;

@Data
public class RecordParam {
    /**
     * 发起者uid
     */
    private String guid;
    /**
     * 目标uid
     */
    private String target;
    /**
     * 消息ID
     */
    private String msgId;
    /**
     * 是否包含语音消息
     */
    private Boolean audio;
}
