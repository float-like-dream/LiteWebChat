package com.socket.client.feign.param;

import lombok.Data;

@Data
public class ShieldParam {
    /**
     * 用户Id
     */
    private String guid;
}
