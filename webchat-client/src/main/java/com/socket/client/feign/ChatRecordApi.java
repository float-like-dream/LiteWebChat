package com.socket.client.feign;

import com.socket.client.feign.param.RecordParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "webchat-server", path = "/message")
public interface ChatRecordApi {
    /**
     * 同步指定用户所有消息为已读
     */
    @PostMapping("/reading")
    void readAllMessage(RecordParam param);
}
