package com.socket.client.util;

import com.socket.core.model.command.BaseCommand;

import java.lang.annotation.Annotation;
import java.util.Optional;
import java.util.function.Function;

/**
 * 命令匹配工具
 */
public class CommandUtil {
    /**
     * 名称匹配能力支持
     *
     * @param command 命令
     * @return 是否支持
     */
    public static <C extends BaseCommand<?>> boolean support(C command, Class<?> clazz) {
        return command.getName().equalsIgnoreCase(clazz.getSimpleName());
    }

    /**
     * 名称与枚举匹配能力支持
     *
     * @param clazz   注解
     * @param command 命令
     * @param func    转换策略
     * @return 是否支持
     */
    public static <A extends Annotation, C extends BaseCommand<?>> boolean support(C command, Class<?> clazz, Class<A> anno, Function<A, C> func) {
        return Optional.ofNullable(clazz.getAnnotation(anno))
                .map(func)
                .map(cmd -> cmd == command)
                .isPresent() || support(command, clazz);
    }
}
