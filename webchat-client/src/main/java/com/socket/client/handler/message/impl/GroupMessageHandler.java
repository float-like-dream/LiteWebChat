package com.socket.client.handler.message.impl;

import com.socket.client.model.chat.ChatMessage;
import com.socket.client.model.chat.ChatUser;
import org.springframework.stereotype.Component;

/**
 * 群组消息执行器
 */
@Component
public class GroupMessageHandler extends UserMessageHandler {
    @Override
    public boolean support(ChatMessage message) {
        return !message.isSysmsg() && message.isGroup();
    }

    /**
     * 发送消息到目标群组
     */
    @Override
    void sendTo(ChatUser self, ChatUser target, ChatMessage message) {
        groupManager.sendGroup(message);
    }
}
