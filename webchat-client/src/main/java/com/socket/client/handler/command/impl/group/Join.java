package com.socket.client.handler.command.impl.group;

import com.socket.client.handler.command.anno.Command;
import com.socket.client.handler.command.impl.GroupCommand;
import com.socket.client.model.chat.ChatUser;
import com.socket.client.model.chat.SysGroup;
import com.socket.client.model.vo.GroupUserVo;
import com.socket.core.model.command.enmus.Group;

/**
 * 加入群组
 */
@Command
public class Join extends GroupCommand {
    @Override
    public void invoke(SysGroup group, ChatUser user) {
        String guid = group.getGuid();
        groupManager.getGroupUsers(guid).add(user);
        // 发送通知
        GroupUserVo data = new GroupUserVo(guid, user.getGuid());
        groupManager.sendGroup(guid, guid, Group.JOIN, data);
    }
}
