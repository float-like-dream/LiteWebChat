package com.socket.client.handler.command.impl;

import com.socket.client.handler.command.CommandHandler;
import com.socket.client.handler.command.anno.Command;
import com.socket.client.manager.UserManager;
import com.socket.client.model.chat.ChatUser;
import com.socket.client.util.CommandUtil;
import com.socket.core.model.command.enmus.User;
import com.socket.core.model.command.topic.UserTopic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 用户信息处理基础类
 */
@Slf4j
public abstract class UserCommand implements CommandHandler<UserTopic, User> {
    @Autowired
    protected UserManager userManager;

    @Override
    public boolean invoke(UserTopic topic) {
        if (support(topic)) {
            log.debug("ParseCommand: {}, Pointer: {}", topic.getOperation(), getClass().getName());
            ChatUser user = userManager.getUser(topic.getTarget());
            this.invoke(user, topic.getParam());
            return true;
        }
        return false;
    }

    @Override
    public boolean support(UserTopic topic) {
        return CommandUtil.support(topic.getOperation(), getClass(), Command.User.class, Command.User::value);
    }

    public abstract void invoke(ChatUser target, String param);
}
