package com.socket.client.handler.command.impl.user;

import com.socket.client.handler.command.anno.Command;
import com.socket.client.handler.command.impl.UserCommand;
import com.socket.client.model.chat.ChatUser;

/**
 * 昵称变更
 */
@Command
public class Name extends UserCommand {
    @Override
    public void invoke(ChatUser target, String param) {
        target.setName(param);
    }
}
