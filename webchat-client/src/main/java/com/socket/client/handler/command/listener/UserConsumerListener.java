package com.socket.client.handler.command.listener;

import cn.hutool.json.JSONUtil;
import com.socket.client.handler.command.CommandHandler;
import com.socket.core.constant.Topics;
import com.socket.core.model.command.enmus.User;
import com.socket.core.model.command.topic.UserTopic;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * 用户事件MQ消费
 */
@Slf4j
@Component
@RequiredArgsConstructor
@RocketMQMessageListener(topic = Topics.USER_COMMAND, consumerGroup = "COMMAND")
public class UserConsumerListener implements RocketMQListener<MessageExt> {
    private final List<CommandHandler<UserTopic, User>> users;

    @Override
    public void onMessage(MessageExt message) {
        String serial = new String(message.getBody());
        log.debug("收到MQ消息：{}", serial);
        UserTopic topic = JSONUtil.parseObj(serial).toBean(UserTopic.class);
        boolean b = users.stream().anyMatch(e -> e.invoke(topic));
        log.debug("命令执行{}: {}", b ? "成功" : "失败", topic.getOperation());
    }
}
