package com.socket.client.handler.command;

import com.socket.core.model.command.BaseCommand;
import com.socket.core.model.command.CommandTopic;

/**
 * Command统一命令处理接口
 */
public interface CommandHandler<T extends CommandTopic<C>, C extends BaseCommand<?>> {
    /**
     * 执行命令
     *
     * @param topic 消息
     * @return 是否成功
     */
    boolean invoke(T topic);

    /**
     * 内置命令匹配方法
     *
     * @param topic 消息
     * @return 是否匹配
     */
    boolean support(T topic);
}
