package com.socket.client.handler.command.impl.group;

import cn.hutool.core.util.StrUtil;
import com.socket.client.handler.command.anno.Command;
import com.socket.client.handler.command.impl.GroupCommand;
import com.socket.client.model.chat.ChatUser;
import com.socket.client.model.chat.SysGroup;
import com.socket.client.model.vo.GroupUserVo;
import com.socket.core.model.command.enmus.Group;

/**
 * 解散群组
 */
@Command
public class Dissolve extends GroupCommand {
    @Override
    public void invoke(SysGroup group, ChatUser user) {
        String guid = group.getGuid();
        String tips = StrUtil.format("群 {} 已被创建者解散", group.getName());
        groupManager.sendGroup(guid, tips, Group.DISSOLVE, new GroupUserVo(guid, null));
        groupManager.remove(guid);
    }
}
