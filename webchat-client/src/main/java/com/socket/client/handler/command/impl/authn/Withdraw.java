package com.socket.client.handler.command.impl.authn;

import com.socket.client.handler.command.anno.Command;
import com.socket.client.handler.command.impl.AuthnCommand;
import com.socket.client.model.base.BaseUser;
import com.socket.client.model.chat.ChatMessage;
import com.socket.client.model.chat.ChatUser;
import com.socket.client.model.chat.SysGroup;
import com.socket.core.model.command.enmus.Authn;

/**
 * 撤回用户消息
 */
@Command
public class Withdraw extends AuthnCommand {
    @Override
    public <T> void invoke(ChatUser self, BaseUser target, T param) {
        // 构建消息
        ChatMessage message = new ChatMessage((String) param, Authn.WITHDRAW);
        message.setGuid(self.getGuid());
        message.setTarget(target.getGuid());
        // 撤回消息
        if (target instanceof SysGroup) {
            withdrawByGroup(message, target);
        } else {
            withdrawByUser(message, self, target);
        }
    }

    /**
     * 通知群组成员撤回消息
     */
    void withdrawByGroup(ChatMessage message, BaseUser target) {
        message.setData(target);
        groupManager.sendGroup(message);
    }

    /**
     * 通知双方撤回此消息
     */
    void withdrawByUser(ChatMessage message, ChatUser self, BaseUser target) {
        message.setData(self);
        target.toUser().send(message);
        self.send(message);
    }
}
