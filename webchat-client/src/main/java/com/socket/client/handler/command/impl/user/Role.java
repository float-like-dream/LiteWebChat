package com.socket.client.handler.command.impl.user;

import com.socket.client.handler.command.anno.Command;
import com.socket.client.handler.command.impl.UserCommand;
import com.socket.client.model.chat.ChatUser;
import com.socket.core.model.command.enmus.User;

/**
 * 设置管理员
 */
@Command
public class Role extends UserCommand {
    @Override
    public void invoke(ChatUser target, String param) {
        target.setRole(param);
        userManager.sendAll(User.ROLE, target);
    }
}
