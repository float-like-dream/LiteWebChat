package com.socket.client.handler.command.impl.authn;

import cn.hutool.core.convert.Convert;
import com.socket.client.handler.command.anno.Command;
import com.socket.client.handler.command.impl.AuthnCommand;
import com.socket.client.model.base.BaseUser;
import com.socket.client.model.chat.ChatUser;
import com.socket.core.model.command.enmus.Authn;
import com.socket.core.util.Wss;

/**
 * 限制登陆
 */
@Command
public class Lock extends AuthnCommand {
    @Override
    public <T> void invoke(ChatUser self, BaseUser target, T param) {
        Long time = Convert.toLong(param);
        if (time > 0) {
            // 注销目标用户
            target.toUser().logout("您已被管理员限制登陆{}", Wss.universal(time));
        }
        // 向所有人发送限制登录通知
        userManager.sendAll(String.valueOf(time), Authn.LOCK, target);
    }
}
