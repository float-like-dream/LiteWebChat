package com.socket.client.handler.command.anno;

import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 系统命令执行标记，必要情况可手动匹配枚举执行
 * （注意：指定枚举优先级高于类名匹配）
 */
@Component
@Retention(RetentionPolicy.RUNTIME)
public @interface Command {

    /**
     * 用户命令执行标记
     */
    @Command
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @interface User {
        com.socket.core.model.command.enmus.User value();
    }

    /**
     * 群组命令执行标记
     */
    @Command
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @interface Group {
        com.socket.core.model.command.enmus.Group value();
    }

    /**
     * 权限命令执行标记
     */
    @Command
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @interface Authn {
        com.socket.core.model.command.enmus.Authn value();
    }
}
