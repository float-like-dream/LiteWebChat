package com.socket.client.handler.message;

import cn.hutool.json.JSONUtil;
import com.socket.client.model.chat.ChatMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 消息解析器支持
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class MessageParser {
    private final List<MessageHandler> handlers;

    /**
     * 解析指定消息
     *
     * @param message 消息
     */
    public void parse(ChatMessage message) {
        if (handlers.stream().noneMatch(e -> e.parse(message))) {
            log.warn("未找到此消息的执行器：{}", JSONUtil.toJsonStr(message));
        }
    }
}
