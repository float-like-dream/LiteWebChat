package com.socket.client.handler.command.impl.group;

import com.socket.client.handler.command.anno.Command;
import com.socket.client.handler.command.impl.GroupCommand;
import com.socket.client.model.chat.ChatUser;
import com.socket.client.model.chat.SysGroup;

import java.util.ArrayList;

/**
 * 创建群组
 */
@Command
public class Create extends GroupCommand {

    @Override
    public void invoke(SysGroup group, ChatUser user) {
        groupManager.put(group.getGuid(), new ArrayList<>());
    }
}
