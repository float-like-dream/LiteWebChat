package com.socket.client.handler.command.impl.authn;

import com.socket.client.handler.command.anno.Command;
import com.socket.client.handler.command.impl.AuthnCommand;
import com.socket.client.model.base.BaseUser;
import com.socket.client.model.chat.ChatUser;
import com.socket.core.model.command.enmus.Authn;

/**
 * 禁言
 */
@Command
public class Mute extends AuthnCommand {
    @Override
    public <T> void invoke(ChatUser self, BaseUser target, T param) {
        // 向所有人发送禁言通知
        userManager.sendAll(String.valueOf(param), Authn.MUTE, target);
    }
}
