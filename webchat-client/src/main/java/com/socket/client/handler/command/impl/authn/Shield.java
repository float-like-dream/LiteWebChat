package com.socket.client.handler.command.impl.authn;

import com.socket.client.handler.command.anno.Command;
import com.socket.client.handler.command.impl.AuthnCommand;
import com.socket.client.model.base.BaseUser;
import com.socket.client.model.chat.ChatUser;
import com.socket.core.model.command.enmus.Authn;

/**
 * 屏蔽
 */
@Command
public class Shield extends AuthnCommand {
    @Override
    public <T> void invoke(ChatUser self, BaseUser target, T param) {
        // 向发起者通知屏蔽结果
        self.send(null, Authn.SHIELD, target);
    }
}
