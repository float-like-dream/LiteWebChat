package com.socket.client.handler.command.impl.group;

import com.socket.client.handler.command.anno.Command;
import com.socket.client.handler.command.impl.GroupCommand;
import com.socket.client.model.chat.ChatUser;
import com.socket.client.model.chat.SysGroup;
import com.socket.client.model.vo.GroupUserVo;
import com.socket.core.model.command.enmus.Group;

import java.util.List;

/**
 * 退出群组
 */
@Command
public class Exit extends GroupCommand {
    @Override
    public void invoke(SysGroup group, ChatUser user) {
        String guid = group.getGuid();
        List<ChatUser> groupUsers = groupManager.getGroupUsers(guid);
        groupUsers.remove(user);
        // 发送通知
        GroupUserVo data = new GroupUserVo(guid, user.getGuid());
        groupManager.sendGroup(guid, null, Group.EXIT, data);
    }
}
