package com.socket.client.handler.message.base;

import cn.hutool.core.util.ClassUtil;
import com.socket.client.model.base.BaseUser;
import com.socket.client.model.chat.ChatMessage;
import com.socket.client.model.chat.ChatUser;
import com.socket.core.model.command.BaseCommand;

/**
 * 基础系统消息执行器
 */
public abstract class BaseSysMessageHandler<E extends BaseCommand<?>> extends BaseMessageHandler {
    /**
     * 解析消息内部方法
     *
     * @param message 消息
     */
    public void parse(ChatMessage message, ChatUser self, BaseUser target) {
        //noinspection unchecked
        this.parse(message, (E) getEmumType(message), self, target);
    }

    @Override
    public boolean support(ChatMessage message) {
        return message.isSysmsg() && getEmumType(message) != null;
    }

    /**
     * 解析消息内部方法
     *
     * @param message 消息
     */
    protected abstract void parse(ChatMessage message, E command, ChatUser self, BaseUser target);

    /**
     * 获取系统消息枚举
     */
    private Object getEmumType(ChatMessage message) {
        Class<?> clazz = ClassUtil.getTypeArgument(getClass());
        for (Object obj : clazz.getEnumConstants()) {
            if (obj.toString().equalsIgnoreCase(message.getType())) {
                return obj;
            }
        }
        return null;
    }
}
