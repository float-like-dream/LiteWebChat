package com.socket.client.handler.command.impl.user;

import com.socket.client.handler.command.anno.Command;
import com.socket.client.handler.command.impl.UserCommand;
import com.socket.client.model.chat.ChatUser;
import com.socket.core.model.command.enmus.User;

/**
 * 设置头衔
 */
@Command
public class Alias extends UserCommand {
    @Override
    public void invoke(ChatUser target, String param) {
        target.setAlias(param);
        userManager.sendAll(param, User.ALIAS, target);
    }
}
