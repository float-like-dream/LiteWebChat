package com.socket.client.handler.command.impl.authn;

import com.socket.client.handler.command.anno.Command;
import com.socket.client.handler.command.impl.AuthnCommand;
import com.socket.client.model.base.BaseUser;
import com.socket.client.model.chat.ChatUser;


/**
 * 永久限制登陆
 */
@Command
public class Forever extends AuthnCommand {
    @Override
    public <T> void invoke(ChatUser self, BaseUser target, T param) {
        target.toUser().logout("您已被管理员永久限制登陆");
        userManager.remove(target.getGuid());
    }
}
