package com.socket.client.handler.command.impl.authn;

import com.socket.client.handler.command.anno.Command;
import com.socket.client.handler.command.impl.AuthnCommand;
import com.socket.client.model.base.BaseUser;
import com.socket.client.model.chat.ChatUser;
import com.socket.core.model.command.enmus.Authn;

/**
 * 发布公告
 */
@Command
public class Announce extends AuthnCommand {
    @Override
    public <T> void invoke(ChatUser self, BaseUser target, T param) {
        userManager.sendAll((String) param, Authn.ANNOUNCE);
    }
}
