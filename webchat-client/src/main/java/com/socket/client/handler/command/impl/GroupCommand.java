package com.socket.client.handler.command.impl;

import com.socket.client.handler.command.CommandHandler;
import com.socket.client.handler.command.anno.Command;
import com.socket.client.manager.GroupManager;
import com.socket.client.manager.UserManager;
import com.socket.client.model.chat.ChatUser;
import com.socket.client.model.chat.SysGroup;
import com.socket.client.util.CommandUtil;
import com.socket.core.model.command.enmus.Group;
import com.socket.core.model.command.topic.GroupTopic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 群组命令处理基础类
 */
@Slf4j
public abstract class GroupCommand implements CommandHandler<GroupTopic, Group> {
    @Autowired
    protected GroupManager groupManager;
    @Autowired
    protected UserManager userManager;

    @Override
    public boolean invoke(GroupTopic topic) {
        if (support(topic)) {
            log.debug("ParseCommand: {}, Pointer: {}", topic.getOperation(), getClass().getName());
            SysGroup group = groupManager.getGroup(topic.getGid());
            ChatUser user = userManager.getUser(topic.getUid());
            this.invoke(group, user);
            return true;
        }
        return false;
    }

    @Override
    public boolean support(GroupTopic topic) {
        return CommandUtil.support(topic.getOperation(), getClass(), Command.Group.class, Command.Group::value);
    }

    public abstract void invoke(SysGroup group, ChatUser user);
}
