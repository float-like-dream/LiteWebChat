package com.socket.client.handler.command.impl.group;

import com.socket.client.handler.command.anno.Command;
import com.socket.client.handler.command.impl.GroupCommand;
import com.socket.client.model.chat.ChatUser;
import com.socket.client.model.chat.SysGroup;
import com.socket.client.model.vo.GroupUserVo;
import com.socket.core.model.command.enmus.Group;

import java.util.List;

/**
 * 移除群组用户
 */
@Command
public class Remove extends GroupCommand {
    @Override
    public void invoke(SysGroup group, ChatUser user) {
        String gid = group.getGuid();
        List<ChatUser> users = groupManager.getGroupUsers(gid);
        users.remove(user);
        // 发送通知
        String uid = user.getGuid();
        GroupUserVo data = new GroupUserVo(gid, uid);
        user.send("您已被管理员移除群聊", Group.REMOVE, data);
    }
}
