package com.socket.client.handler.message.impl;

import com.socket.client.handler.message.base.BaseSysMessageHandler;
import com.socket.client.model.base.BaseUser;
import com.socket.client.model.chat.ChatMessage;
import com.socket.client.model.chat.ChatUser;
import com.socket.core.model.command.enmus.SysLevel;
import com.socket.core.model.command.enmus.WebRTC;
import org.springframework.stereotype.Component;

/**
 * WebRTC消息转发器
 */
@Component
public class WebRTCMessageHandler extends BaseSysMessageHandler<WebRTC> {
    @Override
    protected void parse(ChatMessage message, WebRTC command, ChatUser self, BaseUser target) {
        ChatUser tuser = target.toUser();
        if (authnManager.shield(self, tuser)) {
            self.reject("您已屏蔽目标用户", message);
            return;
        }
        if (authnManager.shield(tuser, self)) {
            self.send("对方屏蔽了你", SysLevel.DANGER);
            return;
        }
        tuser.send(message);
    }
}
