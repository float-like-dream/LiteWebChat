package com.socket.client.handler.message;

import com.socket.client.model.chat.ChatMessage;

/**
 * 标准消息执行接口
 *
 * @date 2023/4/1
 */
public interface MessageHandler {
    /**
     * 解析指定消息
     *
     * @param message 消息
     * @return 是否成功
     */
    boolean parse(ChatMessage message);

    /**
     * 是否支持处理此消息
     *
     * @param message 消息
     * @return 是否支持
     */
    boolean support(ChatMessage message);
}
