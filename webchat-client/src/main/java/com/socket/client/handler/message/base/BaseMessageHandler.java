package com.socket.client.handler.message.base;

import com.socket.client.handler.message.MessageHandler;
import com.socket.client.manager.AuthnManager;
import com.socket.client.manager.GroupManager;
import com.socket.client.manager.UserManager;
import com.socket.client.model.base.BaseUser;
import com.socket.client.model.chat.ChatMessage;
import com.socket.client.model.chat.ChatUser;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 基础消息执行器
 */
public abstract class BaseMessageHandler implements MessageHandler {
    @Autowired
    protected AuthnManager authnManager;
    @Autowired
    protected GroupManager groupManager;
    @Autowired
    protected UserManager userManager;

    /**
     * 解析指定消息
     *
     * @param message 消息
     * @return
     */
    public boolean parse(ChatMessage message) {
        if (support(message)) {
            ChatUser self = userManager.getUser(message.getGuid());
            BaseUser target = authnManager.getBaseUser(message.getTarget());
            this.parse(message, self, target);
            return true;
        }
        return false;
    }

    /**
     * 解析消息内部方法
     *
     * @param message 消息
     */
    protected abstract void parse(ChatMessage message, ChatUser self, BaseUser target);
}
