package com.socket.client.model.chat;

import com.socket.client.model.base.BaseUser;
import com.socket.client.model.enums.OnlineState;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户信息
 */
@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public class SysUser extends BaseUser {
    /**
     * 用户角色
     */
    private String role;
    /**
     * 登录平台
     */
    private String platform;
    /**
     * 头衔
     */
    private String alias;
    /**
     * 是否在线
     */
    private OnlineState online;
}
