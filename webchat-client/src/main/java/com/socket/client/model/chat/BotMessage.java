package com.socket.client.model.chat;

import lombok.Data;

/**
 * AI消息封装
 */
@Data
public class BotMessage {
    /**
     * 单次消息结束标记
     */
    private boolean msgEnd;
    /**
     * 正常单次会话结束
     */
    private boolean singleEnd;
    /**
     * 心跳标识
     */
    private boolean heartbeat;
    /**
     * 语音转换标记
     */
    private boolean audio;
    /**
     * 消息ID
     */
    private String messageId;
    /**
     * 系统提示
     */
    private String tips;
    /**
     * 消息内容
     */
    private String content;
}
