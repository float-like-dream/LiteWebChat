package com.socket.client.model.chat;

import com.socket.client.model.base.BaseUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 群组信息
 */
@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public class SysGroup extends BaseUser {
    /**
     * 所有者
     */
    private String owner;
    /**
     * 群组成员
     */
    private List<String> guids;
}
