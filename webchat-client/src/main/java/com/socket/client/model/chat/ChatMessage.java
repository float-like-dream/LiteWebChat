package com.socket.client.model.chat;

import cn.hutool.crypto.digest.MD5;
import com.socket.core.model.command.BaseCommand;
import com.socket.core.util.Wss;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Socket消息数据
 */
@Data
@NoArgsConstructor
public class ChatMessage {
    /**
     * 系统消息
     */
    private boolean sysmsg;
    /**
     * 拒收消息
     */
    private boolean reject;
    /**
     * 未读消息
     */
    private boolean unread;
    /**
     * 消息唯一id
     */
    private String msgId;
    /**
     * 消息类型
     */
    private String type;
    /**
     * 用户uid
     */
    private String guid;
    /**
     * 目标uid
     */
    private String target;
    /**
     * 消息内容
     */
    private String content;
    /**
     * 附加数据
     */
    private Object data;

    /**
     * 系统消息
     *
     * @param callback 内容
     * @param command  消息类型
     */
    public ChatMessage(String callback, BaseCommand<?> command) {
        this(callback, command, null);
    }

    /**
     * 系统消息
     *
     * @param callback 内容
     * @param command  消息类型
     * @param data     额外数据
     */
    public ChatMessage(String callback, BaseCommand<?> command, Object data) {
        this.sysmsg = true;
        this.content = callback;
        this.type = command.toString();
        this.data = data;
    }

    /**
     * 用户消息
     *
     * @param guid    发起者
     * @param target  目标
     * @param content 内容
     * @param command 消息类型
     */
    public ChatMessage(String guid, String target, String content, BaseCommand<?> command) {
        this.guid = guid;
        this.target = target;
        this.content = content;
        this.type = command.toString();
        this.msgId = generateMsgId();
    }

    /**
     * 生成消息ID
     */
    private String generateMsgId() {
        return MD5.create().digestHex(guid + content + type + target + System.currentTimeMillis());
    }

    /**
     * 消息发送的目标是否为群组
     */
    public boolean isGroup() {
        return Wss.isGroup(target);
    }
}
