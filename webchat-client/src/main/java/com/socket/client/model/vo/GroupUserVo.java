package com.socket.client.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupUserVo {
    /**
     * 群组id
     */
    private String gid;
    /**
     * 用户id
     */
    private String uid;
}
