package com.socket.client.model.base;

import com.socket.client.model.chat.ChatUser;
import com.socket.client.model.chat.SysGroup;
import com.socket.core.util.Wss;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户与群组基础实现
 */
@Data
public abstract class BaseUser {
    /**
     * 唯一id
     */
    @EqualsAndHashCode.Include
    private String guid;
    /**
     * 昵称/群组名称
     */
    private String name;
    /**
     * 头像/群组头像
     */
    private String headimgurl;

    /**
     * 检查此对象是否为群组
     */
    public boolean isGroup() {
        return Wss.isGroup(guid);
    }

    public ChatUser toUser() {
        return this instanceof ChatUser ? (ChatUser) this : null;
    }

    public SysGroup toGroup() {
        return this instanceof SysGroup ? (SysGroup) this : null;
    }
}
