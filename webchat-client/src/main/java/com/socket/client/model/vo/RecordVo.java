package com.socket.client.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class RecordVo {
    /**
     * 消息类型
     */
    private String type;
    /**
     * 发起者
     */
    private String guid;
    /**
     * 接收者
     */
    private String target;
    /**
     * 消息内容
     */
    private String content;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
}
