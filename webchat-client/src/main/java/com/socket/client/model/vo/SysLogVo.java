package com.socket.client.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class SysLogVo {
    /**
     * 触发的用户id
     */
    private String guid;
    /**
     * IP地址
     */
    private String clientIp;
    /**
     * 登录平台
     */
    private String platform;
    /**
     * 日志类型
     */
    private String type;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
}
