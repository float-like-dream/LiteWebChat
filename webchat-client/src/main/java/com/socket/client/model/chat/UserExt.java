package com.socket.client.model.chat;

import lombok.Data;

/**
 * 用户信息扩展
 */
@Data
public class UserExt {
    /**
     * 认证令牌
     */
    private String token;
    /**
     * 客户端IP地址
     */
    private String clientIp;
}
