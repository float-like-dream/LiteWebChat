package com.socket.server.controller.user;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.socket.core.enums.UserRole;
import com.socket.core.manage.SocketRedisManager;
import com.socket.core.model.Announce;
import com.socket.core.model.command.enmus.Authn;
import com.socket.core.model.command.enmus.User;
import com.socket.core.util.Wss;
import com.socket.secure.exception.InvalidRequestException;
import com.socket.secure.util.Assert;
import com.socket.server.custom.OwnerSetting;
import com.socket.server.custom.publisher.CommandPublisher;
import com.socket.server.model.condition.MessageCondition;
import com.socket.server.model.condition.SettingCondition;
import com.socket.server.model.condition.UserCondition;
import com.socket.server.model.enmus.HttpStatus;
import com.socket.server.model.po.ChatRecord;
import com.socket.server.model.po.SysUser;
import com.socket.server.service.ChatRecordService;
import com.socket.server.service.SysUserService;
import com.socket.server.util.ShiroUser;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/owner")
public class OwnerController {
    private final ChatRecordService chatRecordService;
    private final SysUserService sysUserService;
    private final SocketRedisManager redisManager;
    private final CommandPublisher publisher;
    private final OwnerSetting setting;

    @ModelAttribute
    public void checkAuthn() {
        Assert.isTrue(Wss.isOwner(ShiroUser.get().getRole()), "权限不足", InvalidRequestException::new);
    }

    @PostMapping("/alias")
    public void alias(@RequestBody UserCondition condition) {
        String guid = condition.getGuid();
        String content = condition.getContent();
        sysUserService.updateAlias(guid, content);
        publisher.pushUserEvent(guid, content, User.ALIAS);
    }

    @PostMapping("/role")
    public void role(@RequestBody UserCondition condition) {
        String guid = condition.getGuid();
        UserRole role = sysUserService.switchRole(guid);
        ShiroUser.set(SysUser::getRole, role);
        publisher.pushUserEvent(guid, role.getRole(), User.ROLE);
    }

    @PostMapping("/announce")
    public void announce(@RequestBody Announce announce) {
        String content = announce.getContent();
        redisManager.pushNotice(content);
        if (StrUtil.isNotEmpty(content)) {
            publisher.pushAuthnEvent(content, Authn.ANNOUNCE);
        }
    }

    @PostMapping("/remove/user")
    public HttpStatus removeUser(@RequestBody UserCondition condition) {
        LambdaUpdateWrapper<SysUser> wrapper = Wrappers.lambdaUpdate();
        String guid = condition.getGuid();
        wrapper.eq(SysUser::getGuid, guid);
        wrapper.set(SysUser::isDeleted, 1);
        boolean update = sysUserService.update(wrapper);
        if (update) {
            publisher.pushAuthnEvent(guid, null, Authn.FOREVER);
        }
        return HttpStatus.of(update, "操作成功", "找不到此用户");
    }

    @PostMapping("/remove/message")
    public HttpStatus removeMessage(@RequestBody MessageCondition condition) {
        LambdaUpdateWrapper<ChatRecord> wrapper = Wrappers.lambdaUpdate();
        wrapper.eq(ChatRecord::getMsgId, condition.getMsgId());
        // 所有者可直接移除消息
        boolean state = chatRecordService.remove(wrapper);
        return HttpStatus.of(state, "操作成功", "找不到相关记录");
    }

    @PostMapping("/switchSetting")
    public HttpStatus switchSetting(@RequestBody SettingCondition condition) {
        setting.switchSetting(condition.getSetting());
        return HttpStatus.SUCCESS.body();
    }

    @GetMapping("/setting")
    public HttpStatus getSetting() {
        return HttpStatus.SUCCESS.body(setting.getRedisMap());
    }
}
