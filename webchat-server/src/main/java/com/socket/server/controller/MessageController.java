package com.socket.server.controller;

import com.socket.server.model.condition.MessageCondition;
import com.socket.server.model.enmus.HttpStatus;
import com.socket.server.model.po.ChatRecord;
import com.socket.server.service.ChatRecordService;
import com.socket.server.util.ShiroUser;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/message")
public class MessageController {
    private final ChatRecordService chatRecordService;

    @PostMapping("/clear")
    public HttpStatus clear(@RequestBody MessageCondition condition) {
        chatRecordService.removeAllMessage(condition.getTarget());
        return HttpStatus.SUCCESS.message("清除成功");
    }

    @PostMapping("/reading")
    public HttpStatus reading(@RequestBody MessageCondition condition) {
        if (condition.getMsgId() != null) {
            chatRecordService.readMessage(condition.getMsgId(), condition.getTarget());
        } else {
            boolean audio = Boolean.TRUE.equals(condition.getAudio());
            chatRecordService.readAllMessage(ShiroUser.getUserId(), condition.getTarget(), audio);
        }
        return HttpStatus.SUCCESS.message("操作成功");
    }

    @PostMapping("/withdraw")
    public HttpStatus withdrawMessage(@RequestBody MessageCondition condition) {
        boolean state = chatRecordService.withdrawMessage(condition.getMsgId());
        return HttpStatus.state(state, "操作");
    }

    @PostMapping("/remove")
    public HttpStatus removeMessage(@RequestBody MessageCondition condition) {
        boolean state = chatRecordService.removeMessage(condition.getMsgId());
        return HttpStatus.state(state, "操作");
    }

    @GetMapping
    public HttpStatus records(String msgId, String target) {
        List<ChatRecord> list = chatRecordService.getRecords(msgId, target);
        return HttpStatus.SUCCESS.body(list);
    }

    @PostMapping("/preview")
    public HttpStatus getPreviewMap() {
        String userId = ShiroUser.getUserId();
        List<ChatRecord> object = chatRecordService.getPreviewList(userId);
        return HttpStatus.SUCCESS.body(object);
    }
}
