package com.socket.server.controller;

import com.socket.core.model.command.enmus.Authn;
import com.socket.server.custom.publisher.CommandPublisher;
import com.socket.server.model.condition.UserCondition;
import com.socket.server.model.enmus.HttpStatus;
import com.socket.server.service.ShieldUserService;
import com.socket.server.util.ShiroUser;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/shield")
public class ShieldUserController {
    private final ShieldUserService shieldUserService;
    private final CommandPublisher publisher;

    @PostMapping("/shield")
    public HttpStatus shield(@RequestBody UserCondition condition) {
        String guid = Optional.ofNullable(condition.getGuid()).orElseGet(ShiroUser::getUserId);
        boolean b = shieldUserService.shieldTarget(guid);
        publisher.pushAuthnEvent(guid, null, Authn.SHIELD);
        return HttpStatus.of(b, "屏蔽成功", "取消屏蔽");
    }

    @PostMapping("/list")
    public HttpStatus list(@RequestBody UserCondition condition) {
        List<String> list = shieldUserService.getShieldList(condition.getGuid());
        return HttpStatus.SUCCESS.body(list);
    }
}
