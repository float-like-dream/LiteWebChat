package com.socket.server.controller.user;

import com.socket.core.manage.SocketRedisManager;
import com.socket.core.model.command.enmus.Authn;
import com.socket.core.util.Wss;
import com.socket.secure.exception.InvalidRequestException;
import com.socket.secure.util.Assert;
import com.socket.server.custom.publisher.CommandPublisher;
import com.socket.server.model.condition.LimitCondition;
import com.socket.server.util.ShiroUser;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/admin")
public class AdminController {
    private final SocketRedisManager redisManager;
    private final CommandPublisher publisher;

    @ModelAttribute
    public void checkAuthn() {
        Assert.isTrue(Wss.isAdmin(ShiroUser.get().getRole()), "权限不足", InvalidRequestException::new);
    }

    @PostMapping("/mute")
    public void mute(@RequestBody LimitCondition condition) {
        String guid = condition.getGuid();
        Long time = condition.getTime();
        redisManager.setMute(guid, time);
        publisher.pushAuthnEvent(guid, time, Authn.MUTE);
    }

    @PostMapping("/lock")
    public void lock(@RequestBody LimitCondition condition) {
        String guid = condition.getGuid();
        Long time = condition.getTime();
        redisManager.setLock(guid, time);
        publisher.pushAuthnEvent(guid, time, Authn.LOCK);
    }
}
