package com.socket.server.controller;

import com.socket.core.util.Enums;
import com.socket.server.model.enmus.HttpStatus;
import com.socket.server.model.enmus.ProessState;
import com.socket.server.model.po.SysProess;
import com.socket.server.model.vo.SysProessVo;
import com.socket.server.service.SysProessService;
import com.socket.server.util.ShiroUser;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/proess")
public class ProessController {
    private final SysProessService proessService;

    @PostMapping("/apply")
    public HttpStatus apply(@RequestBody SysProessVo proess) {
        boolean apply = proessService.apply(ShiroUser.getUserId(), proess.getTarget(), proess.getReason());
        return HttpStatus.of(apply, "申请已发送", "申请失败");
    }

    @PostMapping("/delete")
    public HttpStatus delete(@RequestBody SysProessVo proess) {
        boolean delete = proessService.delete(ShiroUser.getUserId(), proess.getTarget());
        return HttpStatus.state(delete, "删除");
    }

    @GetMapping("/applyList")
    public HttpStatus applyList() {
        List<SysProess> list = proessService.applyList();
        return HttpStatus.SUCCESS.body(list);
    }

    @PostMapping("/state")
    public HttpStatus state(@RequestBody SysProessVo proess) {
        ProessState of = Enums.of(ProessState.class, proess.getState());
        boolean state = proessService.state(ShiroUser.getUserId(), proess.getTarget(), of);
        return HttpStatus.state(state, "操作");
    }

    @GetMapping("/userList")
    public HttpStatus userList() {
        List<SysProess> list = proessService.userList();
        return HttpStatus.SUCCESS.body(list);
    }
}
