package com.socket.server.controller.user;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.socket.core.enums.RedisTree;
import com.socket.core.model.Announce;
import com.socket.core.util.RedisClient;
import com.socket.secure.util.Assert;
import com.socket.server.custom.exception.BizException;
import com.socket.server.model.condition.EmailCondition;
import com.socket.server.model.condition.UserCondition;
import com.socket.server.model.enmus.HttpStatus;
import com.socket.server.model.vo.SysUserVo;
import com.socket.server.service.SysUserService;
import com.socket.server.util.ShiroUser;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.SecurityUtils;
import org.springframework.data.redis.support.collections.RedisMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Objects;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final SysUserService sysUserService;
    private final RedisClient<String> redis;

    @PostMapping("/material")
    public HttpStatus material(@RequestBody SysUserVo vo) {
        sysUserService.updateMaterial(vo);
        return HttpStatus.SUCCESS.message("修改成功");
    }

    @PostMapping("/avatar")
    public HttpStatus updateAvatar(MultipartFile blob) throws IOException {
        String mapping = sysUserService.updateAvatar(blob.getBytes());
        return HttpStatus.SUCCESS.body("修改成功", mapping);
    }

    @PostMapping("/email")
    public HttpStatus updateEmail(@RequestBody EmailCondition condition) {
        sysUserService.updateEmail(condition);
        return HttpStatus.SUCCESS.message("修改成功");
    }

    @PostMapping("/logout")
    public HttpStatus logout() {
        SecurityUtils.getSubject().logout();
        return HttpStatus.SUCCESS.body();
    }

    @PostMapping("/detail")
    public HttpStatus userDetail(@RequestBody UserCondition condition) {
        String guid = StrUtil.emptyToDefault(condition.getGuid(), ShiroUser.getUserId());
        Assert.notNull(guid, "无效的GUID参数", BizException::new);
        SysUserVo user = sysUserService.getUserInfo(guid, condition.isHideProvince());
        return HttpStatus.SUCCESS.body(user);
    }

    @GetMapping("/announce")
    public HttpStatus getAnnounce(String digest) {
        RedisMap<String, ?> map = redis.withMap(RedisTree.ANNOUNCE);
        Announce announce = BeanUtil.toBean(map, Announce.class);
        // 散列id不同 表示发布新内容
        if (announce != null && !Objects.equals(digest, announce.getDigest())) {
            return HttpStatus.SUCCESS.body(announce);
        }
        return HttpStatus.FAILURE.body();
    }

    @PostMapping("/vaild")
    public HttpStatus vaild() {
        String userId = ShiroUser.getUserId();
        return HttpStatus.SUCCESS.body(userId);
    }
}
