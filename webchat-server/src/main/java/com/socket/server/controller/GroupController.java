package com.socket.server.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.socket.core.constant.ChatConstants;
import com.socket.secure.util.Assert;
import com.socket.server.custom.exception.BizException;
import com.socket.server.model.base.UserPo;
import com.socket.server.model.condition.GroupCondition;
import com.socket.server.model.enmus.HttpStatus;
import com.socket.server.model.po.SysGroup;
import com.socket.server.service.SysGroupService;
import com.socket.server.service.SysProessService;
import com.socket.server.util.ShiroUser;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/group")
public class GroupController {
    private final SysProessService sysProessService;
    private final SysGroupService sysGroupService;
    private final ChatConstants constants;

    @PostMapping("/join")
    public HttpStatus joinGroup(@RequestBody GroupCondition condition) {
        String password = condition.getPassword();
        sysGroupService.joinGroup(condition.getGid(), ShiroUser.getUserId(), password);
        return HttpStatus.SUCCESS.body("申请成功");
    }

    @PostMapping("/exit")
    public HttpStatus exitGroup(@RequestBody GroupCondition condition) {
        String gid = condition.getGid();
        Assert.isFalse(constants.getDefaultGroup().equals(gid), "无法退出默认群组", BizException::new);
        boolean b = sysGroupService.exitGroup(gid);
        return HttpStatus.of(b, "退出成功", "找不到相关信息");
    }

    @PostMapping("/create")
    public HttpStatus createGroup(@RequestBody GroupCondition condition) {
        String gid = sysGroupService.createGroup(condition.getGroupName(), condition.getPassword());
        return HttpStatus.SUCCESS.body("创建成功", gid);
    }

    @PostMapping("/remove")
    public HttpStatus removeUser(@RequestBody GroupCondition condition) {
        String gid = condition.getGid();
        Assert.isFalse(constants.getDefaultGroup().equals(gid), "无法移除默认群组用户", BizException::new);
        boolean b = sysGroupService.removeUser(gid, condition.getUid());
        return HttpStatus.of(b, "移除群组用户成功", "找不到相关信息");
    }

    @PostMapping("/dissolve")
    public HttpStatus dissolve(@RequestBody GroupCondition condition) {
        String gid = condition.getGid();
        Assert.isFalse(constants.getDefaultGroup().equals(gid), "无法解散默认群组", BizException::new);
        boolean b = sysGroupService.dissolveGroup(gid);
        return HttpStatus.of(b, "群组解散成功", "找不到相关信息");
    }

    @PostMapping("/search")
    public HttpStatus search(GroupCondition condition) {
        LambdaQueryWrapper<SysGroup> wrapper = Wrappers.lambdaQuery();
        String gid = condition.getGid();
        wrapper.like(StrUtil.isNotEmpty(gid), SysGroup::getGuid, gid);
        List<SysGroup> list = sysGroupService.list(wrapper);
        return HttpStatus.SUCCESS.body(list);
    }

    @PostMapping("/updatePass")
    public HttpStatus updatePassword(@RequestBody GroupCondition condition) {
        boolean state = sysGroupService.updatePassword(condition.getGid(), condition.getPassword());
        return HttpStatus.state(state, "操作");
    }

    @PostMapping("/detail")
    public HttpStatus groupDetail(@RequestBody GroupCondition condition) {
        String gid = condition.getGid();
        // 查询群组
        LambdaQueryWrapper<SysGroup> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UserPo::getGuid, gid);
        SysGroup group = sysGroupService.getFirst(wrapper);
        Assert.notNull(group, "群组不存在", BizException::new);
        // 查询群组用户
        group.setGuids(sysProessService.groupGuids(gid));
        return HttpStatus.SUCCESS.body(group);
    }
}
