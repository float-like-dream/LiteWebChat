package com.socket.server.service;

import com.socket.server.model.po.ShieldUser;

import java.util.List;

public interface ShieldUserService extends BaseService<ShieldUser> {

    /**
     * 屏蔽/取消屏蔽 指定用户
     *
     * @param target 目标用户
     * @return 若成功屏蔽返回true, 取消屏蔽返回false
     */
    boolean shieldTarget(String target);

    /**
     * 获取当前用户屏蔽列表
     *
     * @param userId 当前用户id
     * @return 屏蔽列表
     */
    List<String> getShieldList(String userId);
}
