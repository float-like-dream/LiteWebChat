package com.socket.server.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.socket.core.constant.Topics;
import com.socket.server.mapper.SysLogMapper;
import com.socket.server.model.po.SysLog;
import com.socket.server.service.SysLogService;
import lombok.RequiredArgsConstructor;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@RocketMQMessageListener(topic = Topics.LOGGER, consumerGroup = "MESSAGE")
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService, RocketMQListener<MessageExt> {
    @Override
    public void onMessage(MessageExt ext) {
        // 反序列化日志
        SysLog log = JSONUtil.toBean(new String(ext.getBody()), SysLog.class);
        super.save(log);
    }
}
