package com.socket.server.service;

import com.socket.server.model.enmus.ProessState;
import com.socket.server.model.po.SysProess;

import java.util.List;

public interface SysProessService extends BaseService<SysProess> {

    /**
     * 申请加入群组/添加好友
     *
     * @param guid   申请人
     * @param target 目标（用户/群组）
     * @param reason 原因
     * @return 是否成功
     */
    boolean apply(String guid, String target, String reason);

    /**
     * 获取有关当前用户的申请列表 <br>
     * 1.检索其他人添加你为好友的申请人列表
     * 2.检索你作为群所有者的其他人申请入群列表
     *
     * @return 申请列表
     */
    List<SysProess> applyList();

    /**
     * 通过/拒绝 加入群组/添加好友
     *
     * @param approver 审批者
     * @param target   申请者
     * @param state    通过/拒绝
     * @return 是否成功
     */
    boolean state(String approver, String target, ProessState state);

    /**
     * 获取当前用户添加的好友列表（包括群组）
     */
    List<SysProess> userList();

    /**
     * 移除好友（退出群组）
     *
     * @param guid   申请人
     * @param target 目标（用户/群组）
     * @return 是否成功
     */
    boolean delete(String guid, String target);

    /**
     * 获取群组已加入的用户列表
     *
     * @param gid 群组ID
     * @return 用户列表
     */
    List<String> groupGuids(String gid);
}
