package com.socket.server.service.impl;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.socket.core.constant.ChatConstants;
import com.socket.core.constant.ChatProperties;
import com.socket.core.constant.Topics;
import com.socket.core.manage.SocketRedisManager;
import com.socket.core.model.command.enmus.Authn;
import com.socket.core.model.command.enmus.Command;
import com.socket.core.util.DbUtil;
import com.socket.core.util.Wss;
import com.socket.secure.util.Assert;
import com.socket.server.custom.exception.BizException;
import com.socket.server.custom.publisher.CommandPublisher;
import com.socket.server.mapper.ChatRecordDeletedMapper;
import com.socket.server.mapper.ChatRecordMapper;
import com.socket.server.mapper.ChatRecordOffsetMapper;
import com.socket.server.model.base.BasePo;
import com.socket.server.model.po.ChatRecord;
import com.socket.server.model.po.ChatRecordDeleted;
import com.socket.server.model.po.ChatRecordOffset;
import com.socket.server.service.ChatRecordService;
import com.socket.server.util.ShiroUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
@RocketMQMessageListener(topic = Topics.MESSAGE, consumerGroup = "MESSAGE")
public class ChatRecordServiceImpl extends ServiceImpl<ChatRecordMapper, ChatRecord> implements ChatRecordService, RocketMQListener<MessageExt> {
    private final ChatRecordDeletedMapper deletedMapper;
    private final ChatRecordOffsetMapper offsetMapper;
    private final ChatRecordMapper recordMapper;
    private final SocketRedisManager redisManager;
    private final CommandPublisher publisher;
    private final ChatProperties properties;
    private final ChatConstants constants;

    @Override
    public List<ChatRecord> getRecords(String msgId, String target) {
        String userId = ShiroUser.getUserId();
        LambdaQueryWrapper<ChatRecord> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(ChatRecord::getSysmsg, 0);

        // 排除已删除的消息ID
        LambdaQueryWrapper<ChatRecordDeleted> exclude = Wrappers.lambdaQuery();

        // 若查询群组，获取所有目标为群组的消息
        if (Wss.isGroup(target)) {
            wrapper.eq(ChatRecord::getReject, false);
            wrapper.eq(ChatRecord::getTarget, target);
            exclude.eq(ChatRecordDeleted::getTarget, target);
        } else {
            // 反之仅获取相互发送的消息
            wrapper.and(ew -> ew.eq(ChatRecord::getReject, false).or().eq(ChatRecord::getGuid, userId));
            wrapper.and(ew -> ew
                    .eq(ChatRecord::getGuid, userId)
                    .eq(ChatRecord::getTarget, target)
                    .or()
                    .eq(ChatRecord::getGuid, target)
                    .eq(ChatRecord::getTarget, userId));
            exclude.and(ew -> ew
                    .eq(ChatRecordDeleted::getGuid, userId)
                    .eq(ChatRecordDeleted::getTarget, target)
                    .or()
                    .eq(ChatRecordDeleted::getGuid, target)
                    .eq(ChatRecordDeleted::getTarget, userId));
        }

        // 限制起始边界id
        LambdaQueryWrapper<ChatRecordOffset> start = Wrappers.lambdaQuery();
        start.eq(ChatRecordOffset::getGuid, userId);
        start.eq(ChatRecordOffset::getTarget, target);
        ChatRecordOffset limit = offsetMapper.selectOne(start);
        if (limit != null) {
            wrapper.gt(BasePo::getId, limit.getOffset());
            exclude.ge(ChatRecordDeleted::getRecordId, limit.getOffset());
        }

        // 限制结束边界id
        if (msgId != null) {
            // 通过消息ID查询主键id
            LambdaQueryWrapper<ChatRecord> end = Wrappers.lambdaQuery();
            end.eq(ChatRecord::getMsgId, msgId);
            ChatRecord offset = getFirst(end);
            Assert.notNull(offset, "无效的消息ID", BizException::new);
            wrapper.lt(BasePo::getId, offset.getId());
            exclude.le(ChatRecordDeleted::getRecordId, offset.getId());
        }

        // 将单独删除的消息转为集合
        List<Long> deleted = deletedMapper.selectList(exclude)
                .stream()
                .map(ChatRecordDeleted::getRecordId)
                .collect(Collectors.toList());
        wrapper.notIn(!deleted.isEmpty(), ChatRecord::getId, deleted);

        // 限制结果
        wrapper.orderByDesc(ChatRecord::getCreateTime);
        wrapper.last(StrUtil.format("LIMIT {}", properties.getSyncRecordsNums()));
        return list(wrapper);
    }

    @Override
    public boolean withdrawMessage(String msgId) {
        LambdaUpdateWrapper<ChatRecord> wrapper = Wrappers.lambdaUpdate();
        wrapper.eq(ChatRecord::getGuid, ShiroUser.getUserId());
        wrapper.eq(ChatRecord::getMsgId, msgId);
        ChatRecord record = getFirst(wrapper);
        Assert.notNull(record, "服务器消息同步中，请稍后再试", BizException::new);
        // 消息未送达或未超过规定撤回时间
        long second = DateUtil.between(record.getCreateTime(), new Date(), DateUnit.SECOND);
        if (record.getReject() || second <= properties.getWithdrawTime()) {
            wrapper.set(BasePo::isDeleted, 1);
            update(wrapper);
            // 若消息未读 计数器-1
            if (record.getUnread()) {
                redisManager.setUnreadCount(record.getTarget(), record.getGuid(), -1);
            }
            // 通知成员撤回
            if (!record.getReject()) {
                String self = record.getGuid(), target = record.getTarget();
                publisher.pushAuthnEvent(self, target, record.getMsgId(), Authn.WITHDRAW);
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean removeMessage(String msgId) {
        String userId = ShiroUser.getUserId();
        LambdaUpdateWrapper<ChatRecord> wrapper = Wrappers.lambdaUpdate();
        wrapper.eq(ChatRecord::getMsgId, msgId);
        ChatRecord record = getFirst(wrapper);
        Assert.notNull(record, "服务器消息同步中，请稍后再试", BizException::new);
        // 添加删除标记
        ChatRecordDeleted deleted = new ChatRecordDeleted();
        deleted.setGuid(userId);
        // 确保移除的目标不是自己（自己可能是发起者或目标）
        String guid = record.getGuid(), target = record.getTarget();
        deleted.setTarget(Wss.isGroup(target) || guid.equals(userId) ? target : guid);
        deleted.setRecordId(record.getId());
        deleted.setCreateTime(record.getCreateTime());
        return deletedMapper.insert(deleted) == 1;
    }

    @Override
    public void removeAllMessage(String target) {
        String guid = ShiroUser.getUserId();
        QueryWrapper<ChatRecord> wrapper = Wrappers.query();
        wrapper.select(DbUtil.selectMax(BasePo::getId));
        LambdaQueryWrapper<ChatRecord> lambda = wrapper.lambda();
        // 群组特殊条件
        if (Wss.isGroup(target)) {
            lambda.eq(ChatRecord::getTarget, target);
        } else {
            // 私聊查找相互发送的消息
            lambda.eq(ChatRecord::getGuid, guid);
            lambda.eq(ChatRecord::getTarget, target);
            lambda.or();
            lambda.eq(ChatRecord::getGuid, target);
            lambda.eq(ChatRecord::getTarget, guid);
        }
        ChatRecord last = getFirst(lambda);
        if (last != null) {
            LambdaUpdateWrapper<ChatRecordOffset> wrapper1 = Wrappers.lambdaUpdate();
            wrapper1.eq(ChatRecordOffset::getGuid, guid);
            wrapper1.eq(ChatRecordOffset::getTarget, target);
            wrapper1.set(ChatRecordOffset::getOffset, last.getId());
            int update = offsetMapper.update(null, wrapper1);
            if (update == 0) {
                ChatRecordOffset offset = new ChatRecordOffset();
                offset.setGuid(guid);
                offset.setTarget(target);
                offset.setOffset(last.getId());
                offsetMapper.insert(offset);
            }
            // 单条消息设置失效
            LambdaUpdateWrapper<ChatRecordDeleted> wrapper2 = Wrappers.lambdaUpdate();
            wrapper2.eq(ChatRecordDeleted::getTarget, target);
            wrapper2.set(BasePo::isDeleted, 1);
            deletedMapper.update(null, wrapper2);
        }
    }

    @Override
    public void readMessage(String msgId, String target) {
        LambdaUpdateWrapper<ChatRecord> wrapper = Wrappers.lambdaUpdate();
        wrapper.eq(ChatRecord::getUnread, true);
        // 此方法由阅读者调用，目标为此消息的发起者，匹配对应数据库的uid发起者
        wrapper.eq(ChatRecord::getGuid, target);
        wrapper.eq(ChatRecord::getMsgId, msgId);
        wrapper.eq(ChatRecord::getTarget, ShiroUser.getUserId());
        wrapper.set(ChatRecord::getUnread, 0);
        super.update(wrapper);
    }

    @Override
    public void readAllMessage(String guid, String target, boolean audio) {
        LambdaUpdateWrapper<ChatRecord> wrapper = Wrappers.lambdaUpdate();
        wrapper.eq(ChatRecord::getUnread, true);
        // 此方法由阅读者调用，目标为此消息的发起者，匹配对应数据库的uid发起者
        wrapper.eq(ChatRecord::getGuid, target);
        wrapper.eq(ChatRecord::getTarget, guid);
        // 语音消息已读设置
        wrapper.ne(!audio, ChatRecord::getType, Command.AUDIO.toString());
        wrapper.set(ChatRecord::getUnread, 0);
        super.update(wrapper);
        // 清空redis计数器
        redisManager.setUnreadCount(guid, target, 0);
    }

    @Override
    public List<ChatRecord> getPreviewList(String userId) {
        // 获取用户关联的消息
        List<ChatRecord> list = recordMapper.selectPreviewByUser(userId);
        list.removeIf(e -> Wss.isGroup(e.getTarget()));
        list.addAll(recordMapper.selectPreviewByGroup(userId, constants.getDefaultGroup()));
        return list;
    }

    @Override
    public void onMessage(MessageExt ext) {
        ChatRecord record = JSONUtil.toBean(new String(ext.getBody()), ChatRecord.class);
        super.save(record);
    }
}
