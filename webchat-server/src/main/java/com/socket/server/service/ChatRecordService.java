package com.socket.server.service;

import com.socket.server.model.po.ChatRecord;

import java.util.List;

public interface ChatRecordService extends BaseService<ChatRecord> {
    /**
     * 获取最近指定条聊天信息
     *
     * @param msgId  最久的消息ID
     * @param target 目标用户
     * @return 聊天记录
     */
    List<ChatRecord> getRecords(String msgId, String target);

    /**
     * 撤回消息（未送达必定撤回成功）
     *
     * @param msgId 消息ID
     * @return 是否成功
     */
    boolean withdrawMessage(String msgId);

    /**
     * 移除消息（仅自己）
     *
     * @param msgId 消息ID
     * @return 是否成功
     */
    boolean removeMessage(String msgId);

    /**
     * 删除当前用户对于目标用户的所有消息（更新消息标记offset）
     *
     * @param target 目标uid
     */
    void removeAllMessage(String target);

    /**
     * 同步指定消息为已读
     *
     * @param msgId  发信人ID/消息ID
     * @param target 变更已读的uid
     */
    void readMessage(String msgId, String target);

    /**
     * 同步指定用户所有消息为已读
     *
     * @param guid   发起人
     * @param target 变更已读的uid
     * @param audio  是否包括语音消息
     */
    void readAllMessage(String guid, String target, boolean audio);

    /**
     * 获取发送到此用户的所有人的最新消息
     *
     * @param userId 用户uid
     * @return 关联未读消息表
     */
    List<ChatRecord> getPreviewList(String userId);
}
