package com.socket.server.request;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONUtil;
import org.springframework.stereotype.Component;

/**
 * IP地址查询
 */
@Component
public class IPAddrRequest {
    private static final String URL = "https://whois.pconline.com.cn/ipJson.jsp?callback=a&ip={}";

    /**
     * 获取IP地址所在省
     */
    public String getProvince(String ip) {
        String body = HttpRequest.get(StrUtil.format(URL, ip)).execute().body();
        body = body.replace("if(window.a) {a(", "");
        body = StrUtil.sub(body, 0, -4);
        String regionName = JSONUtil.parseObj(body).getStr("pro");
        return StrUtil.emptyToDefault(regionName, "未知");
    }
}
