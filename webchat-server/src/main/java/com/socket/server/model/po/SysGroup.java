package com.socket.server.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.socket.server.model.base.UserPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 群组信息
 */
@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public class SysGroup extends UserPo {
    /**
     * 群所有者uid
     */
    private String owner;
    /**
     * 入群密码
     */
    private String password;
    /**
     * 入群无需申请
     */
    private boolean release;
    /**
     * 群组成员
     */
    @TableField(exist = false)
    private List<String> guids;
}
