package com.socket.server.model.condition;

import lombok.Data;

/**
 * @date 2022/4/12
 */
@Data
public class MessageCondition {
    /**
     * 发起者uid
     */
    private String guid;
    /**
     * 目标uid
     */
    private String target;
    /**
     * 消息ID
     */
    private String msgId;
    /**
     * 是否包含语音消息
     */
    private Boolean audio;
}
