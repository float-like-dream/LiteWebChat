package com.socket.server.model.condition;

import lombok.Data;

/**
 * 视频解析参数
 */
@Data
public class URLCondition {
    /**
     * 视频地址
     */
    private String url;
    /**
     * 消息ID
     */
    private String msgId;
    /**
     * 视频类型
     */
    private String type;
}
