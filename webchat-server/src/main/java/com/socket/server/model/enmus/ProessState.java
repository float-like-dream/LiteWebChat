package com.socket.server.model.enmus;

import lombok.Getter;

/**
 * 处理状态枚举
 */
public enum ProessState {
    /**
     * 待处理
     */
    UNTREAT(0),
    /**
     * 通过
     */
    RESOLVE(1),
    /**
     * 拒绝
     */
    REJECT(2);

    @Getter
    private final int code;

    ProessState(int code) {
        this.code = code;
    }
}
