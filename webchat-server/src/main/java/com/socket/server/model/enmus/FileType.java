package com.socket.server.model.enmus;

import com.socket.core.util.Enums;
import lombok.Getter;

/**
 * 文件规范枚举
 *
 * @date 2022/6/8
 */
@Getter
public enum FileType {
    /**
     * 语音
     */
    AUDIO(3 * 1024 * 1024),
    /**
     * 图片
     */
    IMAGE(1024 * 1024),
    /**
     * 视频/文件
     */
    BLOB(100 * 1024 * 1024);

    private final String key;
    private final int size;

    FileType(int size) {
        this.key = Enums.key(this);
        this.size = size;
    }
}
