package com.socket.server.model.condition;

import lombok.Data;

/**
 * @date 2022/5/19
 */
@Data
public class UserCondition {
    /**
     * 用户UID
     */
    private String guid;
    /**
     * 要变更的数据
     */
    private String content;
    /**
     * 是否隐藏所属省份
     */
    private boolean hideProvince;
}
