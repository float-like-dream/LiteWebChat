package com.socket.server.model.base;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 用户与群组基础实现
 */
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class UserPo extends BasePo implements Serializable {
    /**
     * 唯一id
     */
    @EqualsAndHashCode.Include
    private String guid;
    /**
     * 昵称/群组名称
     */
    private String name;
    /**
     * 头像/群组头像
     */
    private String headimgurl;
}
