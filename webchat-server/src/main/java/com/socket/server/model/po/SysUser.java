package com.socket.server.model.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.socket.server.model.base.UserPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 用户信息
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class SysUser extends UserPo {
    /**
     * 散列密码
     */
    private String hash;
    /**
     * 角色
     */
    private String role;
    /**
     * 绑定邮箱
     */
    private String email;
    /**
     * 微信openid
     */
    private String openid;
    /**
     * QQ uin
     */
    private String uin;
    /**
     * 手机
     */
    private String phone;
    /**
     * 生日
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date birth;
    /**
     * 性别（1男 2女）
     */
    private Integer sex;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 头衔
     */
    private String alias;
}
