package com.socket.server.model.vo;

import com.socket.server.model.enmus.ProessState;
import lombok.Data;

@Data
public class SysProessVo {
    /**
     * 申请目标
     */
    private String target;
    /**
     * 申请原因
     */
    private String reason;
    /**
     * 通过/拒绝，请参考{@link ProessState#name()}
     */
    private String state;
}
