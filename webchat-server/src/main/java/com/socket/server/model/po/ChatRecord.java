package com.socket.server.model.po;

import com.socket.server.model.base.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.util.function.Function;

/**
 * 消息映射表
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class ChatRecord extends BasePo implements Comparable<ChatRecord> {
    /**
     * 系统消息
     */
    private Boolean sysmsg;
    /**
     * 是否未读
     */
    private Boolean unread;
    /**
     * 是否拒收
     */
    private Boolean reject;
    /**
     * 发信人uid
     */
    private String guid;
    /**
     * 消息唯一标识
     */
    @EqualsAndHashCode.Include
    private String msgId;
    /**
     * 消息类型
     */
    private String type;
    /**
     * 附加数据
     */
    private Object data;
    /**
     * 收信人uid
     */
    private String target;
    /**
     * 消息内容
     */
    private String content;

    @Override
    public int compareTo(@NonNull ChatRecord record) {
        Function<ChatRecord, Integer> comp = e -> Math.toIntExact(e.getCreateTime().getTime());
        return Integer.compare(comp.apply(record), comp.apply(this));
    }
}
