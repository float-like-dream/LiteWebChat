package com.socket.server.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 用户信息vo
 */
@Data
public class SysUserVo {
    /**
     * 唯一id
     */
    private String guid;
    /**
     * 昵称
     */
    private String name;
    /**
     * 头像/群组头像
     */
    private String headimgurl;
    /**
     * IP所属省
     */
    private String province;
    /**
     * 角色
     */
    private String role;
    /**
     * 绑定邮箱
     */
    private String email;
    /**
     * 手机
     */
    private String phone;
    /**
     * 生日
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date birth;
    /**
     * 性别（1男 2女）
     */
    private Integer sex;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 头衔
     */
    private String alias;
}
