package com.socket.server.model.po;

import com.socket.server.model.base.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统日志表
 *
 * @date 2022/9/29
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysLog extends BasePo {
    /**
     * 用户ID
     */
    private String guid;
    /**
     * 最近登录ip地址
     */
    private String clientIp;
    /**
     * 日志类型
     */
    private String type;
    /**
     * 登录平台
     */
    private String platform;
}
