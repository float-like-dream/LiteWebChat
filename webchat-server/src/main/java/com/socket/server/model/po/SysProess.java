package com.socket.server.model.po;

import com.socket.server.model.base.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 审核信息表
 *
 * @date 2023/5/2
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class SysProess extends BasePo {
    /**
     * 申请人
     */
    private String guid;
    /**
     * 审批人
     */
    private String target;
    /**
     * 申请原因
     */
    private String reason;
    /**
     * 状态（0未处理 1通过 2拒绝）
     */
    private Integer state;

    public SysProess(String guid, String target) {
        this.guid = guid;
        this.target = target;
    }
}
