package com.socket.server.model.condition;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class FileCondition {
    /**
     * Base64格式文件
     */
    private String base64;
    /**
     * 文件
     */
    private MultipartFile blob;
    /**
     * 绑定的消息ID
     */
    private String msgId;
}
