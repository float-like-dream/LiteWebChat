package com.socket.server.model.po;

import com.socket.server.model.base.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 查询偏移量表
 *
 * @date 2022/4/4
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ChatRecordOffset extends BasePo {
    /**
     * 发信人uid
     */
    private String guid;
    /**
     * 收信人uid
     */
    private String target;
    /**
     * 偏移量
     */
    private Long offset;
}
