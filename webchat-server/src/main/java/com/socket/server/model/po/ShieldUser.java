package com.socket.server.model.po;

import com.socket.server.model.base.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 屏蔽用户表
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ShieldUser extends BasePo {
    /**
     * 用户id
     */
    private String guid;
    /**
     * 屏蔽用户id
     */
    private String target;
}
