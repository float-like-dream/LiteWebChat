package com.socket.server.custom.publisher;

import cn.hutool.json.JSONUtil;
import com.socket.core.constant.Topics;
import com.socket.core.model.command.enmus.Authn;
import com.socket.core.model.command.enmus.Group;
import com.socket.core.model.command.enmus.User;
import com.socket.core.model.command.topic.AuthnTopic;
import com.socket.core.model.command.topic.GroupTopic;
import com.socket.core.model.command.topic.UserTopic;
import com.socket.server.model.po.SysGroup;
import com.socket.server.model.po.SysProess;
import com.socket.server.util.ShiroUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.stereotype.Component;

/**
 * 命令推送封装
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class CommandPublisher {
    private final RocketMQTemplate messageQueue;

    public void pushGroupEvent(SysGroup group, Group command) {
        GroupTopic topic = new GroupTopic(group.getGuid(), command);
        String serial = JSONUtil.toJsonStr(topic);
        log.debug("发送MQ消息：{}", serial);
        messageQueue.syncSend(Topics.GROUP_COMMAND, serial);
    }

    public void pushGroupEvent(SysProess user, Group command) {
        GroupTopic topic = new GroupTopic(user.getGuid(), user.getTarget(), command);
        String serial = JSONUtil.toJsonStr(topic);
        log.debug("发送MQ消息：{}", serial);
        messageQueue.syncSend(Topics.GROUP_COMMAND, serial);
    }

    public void pushAuthnEvent(String data, Authn command) {
        pushAuthnEvent(null, data, command);
    }

    public void pushAuthnEvent(String target, Object data, Authn command) {
        pushAuthnEvent(ShiroUser.getUserId(), target, data, command);
    }

    public void pushAuthnEvent(String self, String target, Object data, Authn command) {
        AuthnTopic topic = new AuthnTopic(self, target, data, command);
        String serial = JSONUtil.toJsonStr(topic);
        log.debug("发送MQ消息：{}", serial);
        messageQueue.syncSend(Topics.AUTHN_COMMAND, serial);
    }

    public void pushUserEvent(String data, User command) {
        pushUserEvent(ShiroUser.getUserId(), data, command);
    }

    public void pushUserEvent(String target, String data, User command) {
        UserTopic topic = new UserTopic(target, data, command);
        String serial = JSONUtil.toJsonStr(topic);
        log.debug("发送MQ消息：{}", serial);
        messageQueue.syncSend(Topics.USER_COMMAND, serial);
    }
}
