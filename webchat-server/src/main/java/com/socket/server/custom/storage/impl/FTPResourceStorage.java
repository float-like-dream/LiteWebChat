package com.socket.server.custom.storage.impl;

import cn.hutool.extra.ftp.Ftp;
import cn.hutool.extra.ftp.FtpMode;
import com.socket.server.config.properties.FTPProperties;
import com.socket.server.custom.exception.BizException;
import com.socket.server.custom.storage.ResourceStorage;
import com.socket.server.model.enmus.FileType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * FTP文件资源映射与储存实现 <br>
 * （有关FTP映射文件到nginx请参阅 <a href="https://www.jianshu.com/p/e36e49c248e8">URL</a>）
 */
@Slf4j
@RequiredArgsConstructor
public class FTPResourceStorage implements ResourceStorage {
    private final FTPProperties config;

    @Override
    public String upload(FileType type, byte[] bytes, String hash) {
        String dist = config.getRoot() + "/" + type.getKey();
        String path = dist + "/" + hash;
        try (Ftp session = getFtpSession()) {
            if (session.exist(path) || session.upload(dist, hash, new ByteArrayInputStream(bytes))) {
                return path;
            }
        } catch (IOException e) {
            log.warn("FTP文件上传错误：{}", e.getMessage());
        }
        throw new BizException("FTP文件上传失败：" + path);
    }

    @Override
    public String getOpenURL(String url) {
        return url;
    }

    private Ftp getFtpSession() {
        return new Ftp(config, FtpMode.Passive);
    }
}
