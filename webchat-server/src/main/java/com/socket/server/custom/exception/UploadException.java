package com.socket.server.custom.exception;

/**
 * 文件上传失败异常
 */
public class UploadException extends BizException {
    public UploadException() {
    }

    public UploadException(String s) {
        super(s);
    }
}
