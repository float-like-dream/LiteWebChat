package com.socket.server.custom.filter.anno;

import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.annotation.*;

/**
 * 服务间请求接口标记
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RequestMapping(method = RequestMethod.POST)
public @interface OpenAPI {
    /**
     * @see RequestMapping#value()
     */
    @AliasFor(annotation = RequestMapping.class)
    String[] value() default {};
}
