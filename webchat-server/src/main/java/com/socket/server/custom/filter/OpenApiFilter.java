package com.socket.server.custom.filter;

import cn.hutool.http.HttpStatus;
import com.socket.core.constant.ChatConstants;
import com.socket.secure.util.MappingUtil;
import com.socket.server.custom.filter.anno.OpenAPI;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 服务接口专用过滤器
 */
@Slf4j
@WebFilter
@Component
@RequiredArgsConstructor
public class OpenApiFilter implements Filter {
    private final ChatConstants constants;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest web = WebUtils.toHttp(request);
        HandlerMethod method = MappingUtil.getHandlerMethod(web);
        // 检查服务调用接口
        if (method != null && method.hasMethodAnnotation(OpenAPI.class)) {
            String value = web.getHeader(constants.getOauthToken());
            if (!constants.getOauthTokenValue().equals(value)) {
                WebUtils.toHttp(response).setStatus(HttpStatus.HTTP_NOT_FOUND);
                log.warn("请求认证失败：{}", web.getRequestURI());
                return;
            }
        }
        chain.doFilter(request, response);
    }
}
