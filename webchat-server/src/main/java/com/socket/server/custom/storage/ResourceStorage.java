package com.socket.server.custom.storage;

import cn.hutool.http.HttpRequest;
import com.socket.server.model.enmus.FileType;

/**
 * 资源储存适配接口
 */
public interface ResourceStorage {
    /**
     * 上传指定资源文件
     *
     * @param type  文件类型
     * @param bytes 文件数据
     * @param hash  一致性签名
     * @return 储存的资源地址
     */
    String upload(FileType type, byte[] bytes, String hash);

    /**
     * 获取指定URL的外部访问路径
     *
     * @param url 储存的资源地址
     * @return 原始路径
     */
    String getOpenURL(String url);

    /**
     * 下载指定资源文件
     *
     * @param openURL 开放的资源地址
     * @return 文件数据
     */
    default byte[] download(String openURL) {
        return HttpRequest.get(openURL).execute().bodyBytes();
    }
}
