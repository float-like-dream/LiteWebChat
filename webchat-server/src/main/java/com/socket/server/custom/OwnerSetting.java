package com.socket.server.custom;

import com.socket.core.enums.RedisTree;
import com.socket.core.enums.Setting;
import com.socket.core.util.RedisClient;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.support.collections.RedisMap;
import org.springframework.stereotype.Component;

/**
 * 所有者设置读取和切换
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class OwnerSetting {
    @Getter
    private RedisMap<String, Boolean> redisMap;

    @Autowired
    public void setRedisMap(RedisClient<Boolean> client) {
        this.redisMap = client.withMap(RedisTree.SETTING);
    }

    /**
     * 切换所有者设置的内部方法
     */
    public void switchSetting(Setting setting) {
        String key = setting.getKey();
        redisMap.put(key, !redisMap.getOrDefault(key, false));
    }
}
