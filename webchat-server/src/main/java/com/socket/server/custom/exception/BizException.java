package com.socket.server.custom.exception;

/**
 * 通用业务异常
 */
public class BizException extends RuntimeException {
    public BizException() {
    }

    public BizException(String s) {
        super(s);
    }
}
