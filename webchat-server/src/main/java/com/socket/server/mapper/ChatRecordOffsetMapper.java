package com.socket.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.socket.server.model.po.ChatRecordOffset;
import org.springframework.stereotype.Repository;

@Repository
public interface ChatRecordOffsetMapper extends BaseMapper<ChatRecordOffset> {
}
