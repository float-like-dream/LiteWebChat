package com.socket.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.socket.server.model.po.ChatRecordDeleted;
import org.springframework.stereotype.Repository;

@Repository
public interface ChatRecordDeletedMapper extends BaseMapper<ChatRecordDeleted> {
}
