package com.socket.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.socket.server.model.po.SysProess;
import org.springframework.stereotype.Repository;

@Repository
public interface SysProessMapper extends BaseMapper<SysProess> {
}
