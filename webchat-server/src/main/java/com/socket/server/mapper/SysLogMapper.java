package com.socket.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.socket.server.model.po.SysLog;
import org.springframework.stereotype.Repository;

@Repository
public interface SysLogMapper extends BaseMapper<SysLog> {
}
