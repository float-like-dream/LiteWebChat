package com.socket.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.socket.server.model.po.SysGroup;
import org.springframework.stereotype.Repository;

@Repository
public interface SysGroupMapper extends BaseMapper<SysGroup> {
}
