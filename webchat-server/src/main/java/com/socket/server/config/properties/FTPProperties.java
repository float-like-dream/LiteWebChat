package com.socket.server.config.properties;

import cn.hutool.extra.ftp.FtpConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * FTP对外开放的配置
 */
@Data
@Component
@RefreshScope
@EqualsAndHashCode(callSuper = true)
@ConfigurationProperties(prefix = "ftp.client")
public class FTPProperties extends FtpConfig {
    /**
     * 储存根目录
     */
    private String root;
}
