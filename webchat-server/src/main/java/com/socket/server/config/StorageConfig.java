package com.socket.server.config;

import com.socket.server.config.properties.AListProperties;
import com.socket.server.config.properties.FTPProperties;
import com.socket.server.custom.storage.ResourceStorage;
import com.socket.server.custom.storage.impl.FTPResourceStorage;
import com.socket.server.request.AListRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 资源储存自动装配
 */
@Slf4j
@Configuration
@RequiredArgsConstructor
public class StorageConfig {
    private final AListProperties properties;
    private final FTPProperties config;

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(prefix = "ftp.client", name = "host")
    public ResourceStorage ftpResourceStorage() {
        log.info("装载资源储存服务：{}", FTPResourceStorage.class.getName());
        return new FTPResourceStorage(config);
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(prefix = "alist.config", name = "host")
    public ResourceStorage alistCloudRequest() {
        log.info("装载资源储存服务：{}", AListRequest.class.getName());
        return new AListRequest(properties);
    }
}
