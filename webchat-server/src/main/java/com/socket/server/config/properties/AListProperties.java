package com.socket.server.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * a-list认证配置
 */
@Data
@Component
@RefreshScope
@ConfigurationProperties("alist.config")
public class AListProperties {
    /**
     * AList开放地址
     */
    private String host;
    /**
     * 账号
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 保存文件的根路径
     */
    private String root;

    public String getAuthURL() {
        return host + "/api/auth/login";
    }

    public String getUploadURL() {
        return host + "/api/fs/form";
    }

    public String getDownloadURL() {
        return host + "/api/fs/get";
    }
}
