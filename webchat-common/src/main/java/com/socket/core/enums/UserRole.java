package com.socket.core.enums;

import com.socket.core.util.Enums;

/**
 * 角色枚举
 */
public enum UserRole {
    /**
     * 所有者
     */
    OWNER,
    /**
     * 管理员
     */
    ADMIN,
    /**
     * 用户
     */
    USER;

    public String getRole() {
        return Enums.key(this);
    }

    public boolean is(String role) {
        return getRole().equals(role);
    }

    @Override
    public String toString() {
        return getRole();
    }
}
