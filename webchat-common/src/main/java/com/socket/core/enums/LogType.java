package com.socket.core.enums;

/**
 * 日志类型
 */
public enum LogType {
    LOGIN, LOGOUT
}
