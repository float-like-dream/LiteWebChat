package com.socket.core.enums;

import com.socket.core.util.Enums;
import lombok.Getter;

/**
 * 所有者部分权限状态枚举
 */
public enum Setting {
    /**
     * 敏感关键词审查
     */
    SENSITIVE_WORDS,
    /**
     * 全员禁言
     */
    ALL_MUTE;

    @Getter
    private final String key;

    Setting() {
        this.key = Enums.key(this);
    }
}
