package com.socket.core.model.command;

/**
 * Topic统一命令声明接口
 *
 * @param <E> 命令枚举
 */
@FunctionalInterface
public interface CommandTopic<E extends BaseCommand<? extends Enum<?>>> {
    /**
     * 获取命令枚举
     */
    E getOperation();
}
