package com.socket.core.model.command.enmus;

import com.socket.core.model.command.BaseCommand;

/**
 * WebRTC命令标识
 */
public enum WebRTC implements BaseCommand<Command> {
    AUDIO,
    VIDEO,
    OFFER,
    ANSWER,
    CANDIDATE,
    LEAVE;

    @Override
    public String toString() {
        return getOpenName();
    }
}
