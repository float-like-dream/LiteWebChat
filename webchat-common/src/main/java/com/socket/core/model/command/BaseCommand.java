package com.socket.core.model.command;

import cn.hutool.core.lang.EnumItem;
import com.socket.core.util.Enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * 系统命令枚举（兼容JSON序列化）
 */
public interface BaseCommand<E extends Enum<E> & EnumItem<E>> extends EnumItem<E> {
    /**
     * 获取对外开放的完整命令名
     */
    default String getOpenName() {
        return getClass().getSimpleName() + '.' + getName();
    }

    /**
     * 获取命令枚举对应实现名称
     */
    default String getName() {
        //noinspection unchecked
        return Enums.key((E) this);
    }

    default int intVal() {
        return -1;
    }

    @Override
    default E fromStr(String str) {
        return Arrays.stream(items())
                .filter(em -> Objects.equals(em.toString(), str))
                .findFirst()
                .orElse(null);
    }
}
