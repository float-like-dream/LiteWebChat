package com.socket.core.model.command.topic;

import com.socket.core.model.command.CommandTopic;
import com.socket.core.model.command.enmus.Authn;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 权限命令执行消息
 */
@Data
@NoArgsConstructor
public class AuthnTopic implements CommandTopic<Authn> {
    /**
     * 命令枚举
     */
    private Authn operation;
    /**
     * 发起者
     */
    private String self;
    /**
     * 目标用户
     */
    private String target;
    /**
     * 附加参数
     */
    private Object param;

    public AuthnTopic(String self, String target, Object param, Authn operation) {
        this.self = self;
        this.target = target;
        this.param = param;
        this.operation = operation;
    }
}
