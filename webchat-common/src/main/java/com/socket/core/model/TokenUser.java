package com.socket.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Token认证用户信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TokenUser {
    /**
     * 账号
     */
    private String uid;
    /**
     * 加密密钥
     */
    private String key;
}
