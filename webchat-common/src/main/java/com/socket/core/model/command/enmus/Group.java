package com.socket.core.model.command.enmus;

import com.socket.core.model.command.BaseCommand;

/**
 * 群组信息变动枚举
 */
public enum Group implements BaseCommand<Group> {
    /**
     * 新增群组
     */
    CREATE,
    /**
     * 解散群组
     */
    DISSOLVE,
    /**
     * 加入群组
     */
    JOIN,
    /**
     * 退出群组
     */
    EXIT,
    /**
     * 移除用户
     */
    REMOVE;

    @Override
    public String toString() {
        return getOpenName();
    }
}
