package com.socket.core.model.command.enmus;

import cn.hutool.core.util.StrUtil;
import com.socket.core.model.command.BaseCommand;
import com.socket.core.util.Enums;
import lombok.Getter;

/**
 * WS基础命令枚举
 */
@Getter
public enum Command implements BaseCommand<Command> {
    /**
     * 当前在线用户列表
     */
    LIST,
    /**
     * 用户加入
     */
    JOIN,
    /**
     * 用户退出
     */
    EXIT,
    /**
     * 选择用户变动事件
     */
    CHOOSE,
    /**
     * 在线状态变动事件
     */
    CHANGE,
    /**
     * 会话关闭
     */
    CLOSE,
    /**
     * 消息类型：文字
     */
    TEXT,
    /**
     * 消息类型：文件
     */
    BLOB("文件"),
    /**
     * 消息类型：图片
     */
    IMAGE("图片消息"),
    /**
     * 消息类型：语音
     */
    AUDIO("语音消息"),
    /**
     * 消息类型：视频
     */
    VIDEO("视频文件");

    private String preview;

    Command() {
    }

    Command(String preview) {
        this.preview = preview;
    }

    /**
     * 获取消息预览
     */
    public static String preview(String type, String content) {
        Command of = Enums.of(Command.class, type);
        return of == TEXT ? content : StrUtil.format("[{}]", of.preview);
    }

    /**
     * SocketMessage#getType()快捷比较方法
     */
    public boolean equals(String type) {
        return toString().equals(type);
    }

    @Override
    public String toString() {
        return getName();
    }
}
