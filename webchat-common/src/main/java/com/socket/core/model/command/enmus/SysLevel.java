package com.socket.core.model.command.enmus;

import com.socket.core.model.command.BaseCommand;

/**
 * 系统通知等级
 */
public enum SysLevel implements BaseCommand<SysLevel> {
    SUCCESS,
    PRIMARY,
    WARNING,
    DANGER,
    INFO;

    @Override
    public String toString() {
        return getName();
    }
}
