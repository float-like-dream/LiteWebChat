package com.socket.core.model.command.enmus;

import com.socket.core.model.command.BaseCommand;

/**
 * 权限变动枚举
 */
public enum Authn implements BaseCommand<Authn> {
    /**
     * 屏蔽
     */
    SHIELD,
    /**
     * 撤回消息
     */
    WITHDRAW,
    /**
     * 禁言
     */
    MUTE,
    /**
     * 限制登录
     */
    LOCK,
    /**
     * 永久限制登陆
     */
    FOREVER,
    /**
     * 推送公告
     */
    ANNOUNCE;

    @Override
    public String toString() {
        return getOpenName();
    }
}
