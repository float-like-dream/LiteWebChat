package com.socket.core.model.command.enmus;

import com.socket.core.model.command.BaseCommand;

/**
 * 用户数据更新枚举
 */
public enum User implements BaseCommand<User> {
    /**
     * 昵称变动
     */
    NAME,
    /**
     * 头像变动
     */
    HEADIMG,
    /**
     * 设置头衔
     */
    ALIAS,
    /**
     * 设置管理员
     */
    ROLE;

    @Override
    public String toString() {
        return getOpenName();
    }
}
