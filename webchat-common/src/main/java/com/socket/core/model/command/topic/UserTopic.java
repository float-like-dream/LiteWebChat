package com.socket.core.model.command.topic;

import com.socket.core.model.command.CommandTopic;
import com.socket.core.model.command.enmus.User;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户状态变更消息
 */
@Data
@NoArgsConstructor
public class UserTopic implements CommandTopic<User> {
    /**
     * 命令枚举
     */
    private User operation;
    /**
     * 触发者
     */
    private String target;
    /**
     * 新数据
     */
    private String param;

    public UserTopic(String target, String param, User operation) {
        this.operation = operation;
        this.param = param;
        this.target = target;
    }
}
