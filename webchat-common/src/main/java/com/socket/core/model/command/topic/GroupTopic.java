package com.socket.core.model.command.topic;

import com.socket.core.model.command.CommandTopic;
import com.socket.core.model.command.enmus.Group;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 群组变更消息
 */
@Data
@NoArgsConstructor
public class GroupTopic implements CommandTopic<Group> {
    /**
     * 命令枚举
     */
    private Group operation;
    /**
     * 群组用户id
     */
    private String uid;
    /**
     * 群组id
     */
    private String gid;

    public GroupTopic(String gid, Group operation) {
        this.gid = gid;
        this.operation = operation;
    }

    public GroupTopic(String gid, String uid, Group operation) {
        this.gid = gid;
        this.uid = uid;
        this.operation = operation;
    }
}
