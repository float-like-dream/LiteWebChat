package com.socket.core.constant;

import java.util.concurrent.TimeUnit;

/**
 * 常量池
 */
public interface Constants {
    /**
     * 微信手机快捷登录标识
     */
    String WX_MOBILE = "$1";
    /**
     * 异地验证成功标记
     */
    String AUHT_OFFSITE_REQUEST = "AUHT_OFFSITE_REQUEST";
    /**
     * 认证密钥标识
     */
    String AUTH_TOKEN = "Token";
    /**
     * TokenAPI附带的用户guid
     */
    String USER_ID = "userId";
    /**
     * Session会话有效时间（单位；秒）
     */
    long SESSION_VALID_TIME = TimeUnit.DAYS.toSeconds(1);
}
