# LiteWebChat

#### 项目介绍

基于最新HTML5技术，实现网页上的即时通信系统，支持安卓/PC浏览器，用户无需安装任何软件/插件就能在网页即时收发消息；可适用于客服，在线讨论区，企业内部交流等场景。<br><br>
支持大部分类型消息的发送和解析，如文字，表情，图片，语音，视频，文件。支持视频录制，视频语音播放（包括音乐类文件），消息撤回，屏蔽用户，管理员权限（禁言，限制登录），所有者权限（设置管理员，设置头衔，发布公告等），个性化设置（背景图自定义，黑暗主题，气泡颜色等），消息通知，来电铃声，全员禁言等功能<br>
使用WebRTC实现视频语音通话技术，同时支持拒接、忽略、屏蔽操作 <br>
公共接口，隐私接口（如聊天记录），ws消息传输均已加密，使用自研加密模块保证消息不被其他人监听和篡改，秘钥交换使用图片伪装，两重时间检查和三重签名检查；模块还支持参数加密，参数校验，重放拦截，时间、IP、UA检查等 [参考文档](https://jaedoq3bux.feishu.cn/docx/Diavddup9oB08Ax8di6c2gU1nnd)。<br>

<b>目前 这个项目已停止维护</b><br>

#### 系统结构

```
                                              |----> UserManager ---------|
--------------                                |                           | ------> SocketRedisManager
webchat-client -------> SocketEndpoint -------|----> AuthnManager --------|
--------------                                |
                                              |----> MessageParser
       ⇑
[webchat-common]       |---> message
       ⇓               |                      | ------> BaiduSpeech
                       |---> resource --------| 
--------------         |                      | ------> ResourceStorage
webchat-server --------|---> group
--------------         |                      |---> admin
                       |---> user ------------|
                       |                      |---> owner
                       |                                       | ------> WX
                       |---> login ----------> shiro ----------|
                                                               | ------> QQ

                                              | -----> CamouflagePublicKey
                       | ---> Exchange -------|
--------------         |                      | -----> SignatureGenerator
webchat-secure --------|                                                                              | -----> MappedRepeatValidator
--------------         |                                              | ----> RepeatValidator --------|
                       | ---> Validator ----> SecureRequestFilter-----|                               | -----> RedisRepeatValidator
                                                                      | ----> SignatureValidator
```

消息解析器

```

                                                | ------> UserMessageHandler --------> GroupMessageHandler
                                                |
[MessageParser] ------> BaseMessageHandler -----|                                     
                                                |                                     | -------> SysMessageHandler
                                                | ------> BaseSysMessageHandler ------|
                                                                                      | -------> WebRTCMessageHandler
```

命令解析器

```
                                                | -----> GroupCommand ------------> GroupManager
                                                |
                                                |                          |------> GroupManager
[ConsumerListener] -----> CommandHandler -------| -----> UserCommand ------|
                                                |                          |------> UserManager
                                                |                                                      | ------> GroupManager
                                                | -----> AuthnCommand ------------> AuthnManager ------|
                                                                                                       | ------> UserManager
```

推荐JDK11

* 后端：Spring Boot、Spring Cloud、Mybatis Plus、Shiro、MySQL、Redis、Kafka、Netty、Nacos、OpenFeign

#### 依赖及功能

* MySQL 5.7.1 <br>
数据持久化储存，包括聊天记录、用户信息、操作日志等 <br>

* Redis 7.0.4 <br>
未读消息计数 <br>
Token令牌缓存及验证 <br>
所有者权限扩展 <br>
公告储存 <br>
ResourceURL资源缓存 <br>
屏蔽列表缓存 <br>
微信UUID缓存及验证 <br>
禁言限制登陆标记 <br>
刷屏统计 <br>
Session持久化 <br>

* Kafka 3.0.1 Topic <br>
MESSAGE - 聊天记录 <br>
LOGGER - 日志推送 <br>
USER_COMMAND - 用户相关命令 <br>
GROUP_COMMAND - 群组相关命令 <br>
AUTHN_COMMAND - 权限相关命令 <br>

* Nacos 2.2.0 <br>
注册中心 <br>
配置中心 <br>

* 系统邮箱账号 <br>
用于修改密码/找回密码功能发送验证码 <br>

* 微信公众号 <br>
微信快捷登录功能

* AList/FTP <br>
文件持久化储存 <br>


#### 扩展功能

* @WeChatRedirect - 微信登录重定向标记，标记此注解的方法将对外开放[GET]请求并允许跨域，通过WxAuth2Request扫描控制器标记的注解，动态配置微信授权地址
* ResourceStorage - 通用资源储存接口，默认实现了两种（AList，FTP），可根据业务需求手动实现储存方式（如FastDFS），Spring会优先装配自定义储存方式
* CommandHandler - 系统命令处理接口，通过命令枚举和Bean名匹配的方式动态执行命令，也可指定@Command注解手动匹配命令

#### 引用
[[AList] 一个支持多种存储的文件列表程序](https://github.com/alist-org/alist) <br>
[[netty-websocket-spring-boot-starter] 基于netty的轻量级的高性能socket服务器](https://github.com/YeautyYE/netty-websocket-spring-boot-starter) <br>